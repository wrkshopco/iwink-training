/**
 * Zorgt ervoor dat functies niet continue aangeroepen worden, maar na N miliseconden.
 *
 * @param {*} func
 * @param {*} wait
 * @param {*} immediate
 */
function debounce(func, wait, immediate) {
  var timeout;
  return function() {
    var context = this,
      args = arguments;
    var later = function() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
}

/**
 * Mobiele Navigatie
 */
(function() {
  var menuToggle = document.querySelector(".nav__toggle");
  var isMenuOpen = false;

  menuToggle.addEventListener("click", function(ele) {
    ele.preventDefault();
    isMenuOpen = !isMenuOpen;

    document.body.classList.toggle("nav__menu--toggled");

    menuToggle.setAttribute("aria-expanded", isMenuOpen);
  });
})();

/**
 * Equal Heights
 *
 * Geeft elementen met className .js--equal-height gelijke hoogte
 */
(function() {
  var equalHeights = debounce(function(className) {
    var findClass = document.getElementsByClassName(className);
    var tallest = 0;

    for (i = 0; i < findClass.length; i++) {
      var ele = findClass[i];
      var eleHeight = ele.offsetHeight;

      tallest = eleHeight > tallest ? eleHeight : tallest;
    }
    for (i = 0; i < findClass.length; i++) {
      findClass[i].style.height = tallest + "px";
    }
  }, 250);

  var w = Math.max(
    document.documentElement.clientWidth,
    window.innerWidth || 0
  );

  /**
   * Voer de functie alleen uit boven 960px breakpoint
   */
  if (w > 960) {
    equalHeights("js--equal-height");
  }

  window.addEventListener("resize", function() {
    if (w > 960) {
      equalHeights("js--equal-height");
    }
  });
})();

/**
 * Fixed header
 *
 * Zorgt ervoor dat de header bij scroll bovenaan de pagina blijft en alternatieve styling krijgt.
 */
(function() {
  "use strict";

  var nav = document.querySelector(".header");
  var hero = document.querySelector(".hero");
  var heroHeight = hero.offsetHeight;
  var headerHeight = nav.offsetHeight;

  var fixNav = debounce(function() {
    // window.pageYOffset ipv. window.offsetY voor x browser compatibility
    if (window.pageYOffset > headerHeight && window.pageYOffset < heroHeight) {
      document.body.classList.add("header__up");
      document.body.classList.remove("header__down");
    } else if (window.pageYOffset > heroHeight) {
      document.body.classList.remove("header__up");
      document.body.classList.add("header__down", "header__fixed");
    } else {
      document.body.classList.remove("header__up", "header__fixed");
    }
  }, 250);

  window.addEventListener("scroll", fixNav);
})();

/**
 * Zoeken overlay
 */
(function() {
  "use strict";

  var searchOverlay = document.querySelector(".search__overlay");
  var searchOpen = document.querySelector("#js__search--open");
  var searchClose = document.querySelector("#js__search--close");
  var searchInput = document.getElementById("search__text");

  var focusOn = function(ele) {
    // Focus het element alleen als het daadwerkelijk zichbaar is
    if (document.body.classList.contains("search__visible")) {
      ele.focus();
    } else {
      // Verwijder anders de focus van het element
      ele.blur();
    }
  };

  var toggleSearch = function() {
    document.body.classList.toggle("search__visible");

    focusOn(searchInput);

    return false;
  };

  searchOpen.addEventListener("click", toggleSearch, false);
  searchClose.addEventListener("click", toggleSearch, false);
})();

/**
 * Google Map
 */
function initMap() {
  var copaCabana = { lat: -22.9732698, lng: -43.2032649 };
  var map = new google.maps.Map(document.getElementById("map"), {
    zoom: 13,
    center: copaCabana,
    scrollwheel: false,
    navigationControl: false,
    mapTypeControl: false,
    scaleControl: false,
    draggable: false
  });
  var marker = new google.maps.Marker({
    position: copaCabana,
    map: map
  });
}

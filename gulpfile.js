var gulp = require("gulp");
var sass = require("gulp-sass");
var useref = require("gulp-useref");
var uglify = require("gulp-uglify");
var gulpIf = require("gulp-if");
var htmlmin = require("gulp-htmlmin");
var cssnano = require("gulp-cssnano");
var autoprefixer = require("gulp-autoprefixer");
var imagemin = require("gulp-imagemin"),
  pngquant = require("imagemin-pngquant");
var cache = require("gulp-cache");
var del = require("del");
var runSequence = require("run-sequence");
var browserSync = require("browser-sync").create();

/**
 * Compileer SASS bestanden en reload met browserSync
 * Include normalize.css uit node_modules
 */
gulp.task("sass", function() {
  return gulp
    .src("src/scss/**/*.scss")
    .pipe(
      sass({
        includePaths: ["node_modules/"]
      })
    )
    .on("error", function(err) {
      console.log(err.toString());
      this.emit("end");
    })
    .pipe(gulp.dest("src/css"))
    .pipe(
      browserSync.reload({
        stream: true
      })
    );
});

/**
 * Compileer geminificeerde JS en CSS bestanden naar build in de correcte volgorde met useref
 */
gulp.task("useref", function() {
  return gulp
    .src("src/*.html")
    .pipe(useref())
    .pipe(gulpIf("*.js", uglify()))
    .on("error", function(err) {
      console.log(err.toString());
      this.emit("end");
    })
    .pipe(
      gulpIf(
        "*.css",
        autoprefixer({
          browsers: ["last 2 versions"],
          cascade: false
        })
      )
    )
    .pipe(gulpIf("*.css", cssnano()))
    .pipe(
      gulpIf(
        "*.html",
        htmlmin({
          collapseWhitespace: true
        })
      )
    )
    .pipe(gulp.dest("dist"));
});

/**
 * Optimaliseer afbeeldingen en cache dit process zodat het niet onnodig wordt herhaald
 */
gulp.task("images", function() {
  return gulp
    .src("src/images/**/*.+(png|jpg|gif|svg)")
    .pipe(
      cache(
        imagemin({
          progressive: true,
          use: [pngquant()]
        })
      )
    )
    .pipe(gulp.dest("dist/images"));
});

/**
 * Ververs de browser wanneer wijzigingen worden gedaan in /src
 */
gulp.task("browserSync", function() {
  browserSync.init({
    server: {
      baseDir: "src"
    }
  });
});

/**
 * Verwijder onnodige bestanden in dist
 */
gulp.task("clean:dist", function() {
  return del.sync("dist");
});

/**
 * Watch wijzigingen en ververs de browser
 */
gulp.task("watch", ["browserSync"], function() {
  gulp.watch("src/scss/**/*.scss", ["sass"]);
  gulp.watch("src/*.html", browserSync.reload);
  gulp.watch("src/js/**/*.js", browserSync.reload);
});

/**
 * Gulp taak zodat je `gulp` kan gebruiken in de terminal
 */
gulp.task("default", function(callback) {
  runSequence(["sass", "browserSync", "watch"], callback);
});

/**
 * Genereer productie bestanden in dist
 */
gulp.task("build", function(callback) {
  runSequence("clean:dist", ["sass", "useref", "images"], callback);
});

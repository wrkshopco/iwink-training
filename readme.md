# iWink Training

Een HTML, CSS en Javascript uitwerking van het iWink Beach Thema.

## Benodigdheden

Voor het project heb ik gebruik gemaakt van NPM voor package management en Gulp voor het build proces.

* [NodeJS](http://nodejs.org) - Gebruik de installer
* [GulpJS](https://github.com/gulpjs/gulp) - `$ npm install -g gulp`

## Installatie

1. `$ git clone git@bitbucket.org:wrkshopco/iwink-training.git` of download hem in een locatie naar keuze.
2. Voer daarna `$ npm install` uit in die locatie. (Dit installeert de benodigde plugins)

## Gebruik

Om de ontwikkelomgeving te starten hoef je alleen `$ gulp` uit te voeren in de project locatie.

## Build task

* Met het commando `$ gulp build` wordt er een productie versie van het project gemaakt.
* Wat gebeurd er met het build commando?
  * Afbeeldingen worden gecompressed
  * HTML, CSS en Javascript wordt geminificeerd en desbetreffende bestanden worden samengevoegd.
